<%@ include file="/WEB-INF/layouts/include.jsp"%>

<h1>Add New Car Part</h1>
<div class="row">
	<div class="col-sm-12">
		<div>${message}</div>
		<form method="post" action="<c:url value='/carparts/partnumber' />">		
			<c:if test="${not empty carpart}">
			    <input type="hidden" id="update" name="update" value="true" />
			</c:if>
			<div class="col-sm-4 form-group">
				<label for="partnumber">Part#</label>
				<input type="text" id="partnumber" name="partnumber" 
					value="${carpart.partNumber}"  />
			</div>
			<div class="col-sm-4 form-group">
				<label for="title">Title</label>
				<input type="text" id="title" name="title" 
					value="${carpart.title}" />
			</div>
			<div class="col-sm-4 form-group">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</form>
		<c:if test="${not empty carpart}">
		    <b>Part Number:</b> ${carpart.partNumber}<br/>
		    <b>Title:</b> ${carpart.title}<br/>
		    <a href="<c:url value='/carparts/delete/${carpart.partNumber}'/>" 
		    	class="btn btn-danger">Delete</a>
		</c:if>
		<c:if test="${not empty carpart}">
		    <h3>New  Carpart Saved</h3>
		    <b>Part Number:</b> ${carpart.partNumber}<br/>
		    <b>Title:</b> ${carpart.title} 
		</c:if>

		
	</div>
</div>
<script>
	orly.ready.then(()=>{
		let message = "${message}";
		let type = "info";
		
		if (message.length > 0) {
			type = ("${messageType}".length > 0) ? "${messageType}" : type;
			orly.qid("alerts").createAlert({type:type, duration:"3000", msg:message});
		}
	});
</script>