package com.oreillyauto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.domain.Employee;
import com.oreillyauto.service.EmployeeService;

@Controller
public class EmployeeController extends BaseController {
    @Autowired
    EmployeeService employeeService;
    
    @ResponseBody
    @GetMapping(value = { "/employees" })
    public List<Employee> getEmployees() {
    	employeeService.runJpaExamples();
        List<Employee> empList = employeeService.getAllEmployees();
        return empList;
    }    
}
