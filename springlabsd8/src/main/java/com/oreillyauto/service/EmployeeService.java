package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.Employee;

public interface EmployeeService {
    void runJpaExamples();

	List<Employee> getAllEmployees();
}
