package com.oreillyauto.service.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.EmployeeRepository;
import com.oreillyauto.domain.Employee;
import com.oreillyauto.service.EmployeeService;
import com.oreillyauto.util.Helper;

@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;

    /**
     * findBy, readBy, queryBy, getBy
     * There is no "real" difference between these four approaches. 
     * These have been provided as options to suit individual preferences 
     * as different developers may have different preferences for how 
     * they would like to name their methods.
     * 
     * Caveat: getBy tends to lean to a reference approach and edge
     *         cases can get you into trouble. Never use getBy
     * 
     * countBy - quick and easy way to get the count
     */
    @Override
    public void runJpaExamples() {
    	System.out.println("");
    	
    	/** GET BY */
    	// Never use get by
    	
    	/** FIND LIST */
    	// Query for a user that exists using find
//    	System.out.println("findByFirstNameAndLastName(\"Bob\", \"Smith\")");
//    	runListJpaExample("find", "Bob", "Smith");
    	
    	// Query for a user that does not exist using find
//    	System.out.println("findByFirstNameAndLastName(\"Smith\", \"Robert\")");
//    	runListJpaExample("find", "Smith", "Robert");
    	
    	/** FIND EMPLOYEE */
    	// Query for a single user that exists using find
//    	System.out.println("findById(1)");
//    	runEmployeeJpaExample("find", 1);
    	
    	// Query for a user that does not exist using find
//    	System.out.println("findById(10)");    	
//    	runEmployeeJpaExample("find", 10);
    	    	
    	/** COUNT EMPLOYEES BY LAST NAME */
//    	System.out.println("countByLastName(\"Smith\")");
//    	Long count = employeeRepository.countByLastName("Smith");
//    	System.out.println("Number of users with last name = \"Smith\": " + count);
    	
    	/** READ LIST */
//    	System.out.println("readByFirstNameAndLastName(\"Smith\", \"Bob\")");
//    	List<Employee> readList = employeeRepository.readByLastNameAndFirstName("Smith", "Bob");
//    	Helper.printList(readList);
    	
    	/** QUERY LIST */
//    	System.out.println("queryByLastNameAndFirstName(\"Johnson\", \"Alison\")");
//    	List<Employee> queryList = employeeRepository.queryByLastNameAndFirstName("Johnson", "Alison");
//    	Helper.printList(queryList);
    	
    	/** BEFORE */
//    	System.out.println("findByStartDateBefore(\"2020-01-01\")");
//    	Date date = Date.valueOf("2020-01-01"); // java.sql.Date
//    	List<Employee> beforeList = employeeRepository.findByStartDateBefore(date);
//    	Helper.printList(beforeList); 	
    	
    	/** LIKE */
//    	System.out.println("findByLastNameLike(\"%n\")");
//    	List<Employee> likeList = employeeRepository.findByLastNameLike("%n");
//    	Helper.printList(likeList);
    	
    	/** IN */
//    	System.out.println("findByLastNameLike(\"%n\")");
//    	List<String> empList = new ArrayList<String>();
//    	empList.add("Bob");
//    	empList.add("Sue");
//    	List<Employee> inList = employeeRepository.findByFirstNameIn(empList);
//    	Helper.printList(inList);
    	
    	// Let's perform more on-the-fly examples in class ... requests?
    	
    	
    	
    	// Data
    	System.out.println("");
    	System.out.println("{\"id\":1,\"firstName\":\"Bob\",\"lastName\":\"Smith\",\"age\":30,\"startDate\":1514786400000}");
    	System.out.println("{\"id\":2,\"firstName\":\"Sue\",\"lastName\":\"Smith\",\"age\":45,\"startDate\":1516428000000}");
    	System.out.println("{\"id\":3,\"firstName\":\"Tim\",\"lastName\":\"Johnson\",\"age\":28,\"startDate\":1518674400000}");
    	System.out.println("{\"id\":4,\"firstName\":\"Alison\",\"lastName\":\"Johnson\",\"age\":22,\"startDate\":1518674400000}");
    	System.out.println("{\"id\":5,\"firstName\":\"Gabe\",\"lastName\":\"Rickman\",\"age\":28,\"startDate\":1483250400000}");
    	System.out.println("{\"id\":6,\"firstName\":\"Jennette\",\"lastName\":\"Sabine\",\"age\":37,\"startDate\":1483250400000}");
    	System.out.println("");
    }

	public void runEmployeeJpaExample(String strategy, Integer id) {
		Optional<Employee> employee = null;
		
		// Query for a user that exists
		switch (strategy) {
		case "find":
			employee = employeeRepository.findById(id);	
			break;
		case "get" : 
			employee = employeeRepository.getById(id);
			break;
		}
    	
    	if (employee.isPresent()) {
    		System.out.println(employee.get());
    	} else {
    		System.out.println("User with id="+id+" Not Found");
    	}
	}
	
	public void runListJpaExample(String strategy, String firstName, String lastName) {
		List<Employee> employeeList = new ArrayList<Employee>();
		
		// Query for a user that exists
		switch (strategy) {
		case "find":
			employeeList = employeeRepository.findByLastNameAndFirstName(lastName, firstName);	
			break;
		case "get" : 
			employeeList = employeeRepository.getByLastNameAndFirstName(lastName, firstName);
			break;
		}
    	
    	if (Helper.hasItems(employeeList)) {
    		for (Employee employee : employeeList) {
                System.out.println(employee);
            }	
    	} else {
    		System.out.println("User "+firstName+" "+lastName+" Not Found");
    	}
	}

	@Override
	public List<Employee> getAllEmployees() {
		return (List<Employee>) employeeRepository.findAll();
	}
}
