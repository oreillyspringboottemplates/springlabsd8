package com.oreillyauto.service.impl;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.CarpartsRepository;
import com.oreillyauto.domain.Carpart;
import com.oreillyauto.dto.Email;
import com.oreillyauto.service.CarpartsService;

@Service
public class CarpartServiceImpl implements CarpartsService {
	@Autowired
	private CarpartsRepository carpartsRepo;

	@Override
	public Carpart getCarpartByPartNumber(String partNumber) {
		// SPRING DATA API
//    	return carpartsRepo.findByPartNumber(partNumber);

		// CRUD LIBRARY/API
		return carpartsRepo.findById(partNumber).get(); // cheat!

		// VIA QUERY DSL
//    	return carpartsRepo.getCarpart(partNumber);
	}

	@Override
	public Carpart getCarpart(String partNumber) throws Exception {
		return carpartsRepo.getCarpart(partNumber);
	}

	@Override
	public boolean saveCarpart(Carpart carpart) {
		Carpart savedCarpart = carpartsRepo.save(carpart);
		return savedCarpart != null;
	}

	@Override
	public void deleteCarpartByPartNumber(Carpart carpart) {
		carpartsRepo.delete(carpart);
	}

	@Override
	public boolean saveCarpart(String partnumber, String title) {
		Carpart carpart = new Carpart();
		carpart.setPartNumber(partnumber);
		carpart.setTitle(title);
		carpart.setLine("");
		carpart.setDescription("");
		Carpart savedCarpart = carpartsRepo.save(carpart);
		return savedCarpart != null;
	}

	@Override
	public void sendEmail(Email email) throws Exception {
		Properties appProperties = new Properties();
		appProperties.load(CarpartServiceImpl.class.getClassLoader().getResourceAsStream("application.properties"));
		final String authEmailAddress = appProperties.getProperty("gmail.username");
		final String authPassword = appProperties.getProperty("gmail.password");
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(authEmailAddress, authPassword);
			}
		});

		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(email.getEmailAddress()));
		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(authEmailAddress));
		message.setSubject("Contact Us Submission");
		message.setText(email.getEmailBody());
		Transport.send(message);
	}

}
