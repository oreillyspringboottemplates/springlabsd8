package com.oreillyauto.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.ExampleRepositoryCustom;
import com.oreillyauto.domain.Employee;
import com.oreillyauto.domain.Example;
import com.oreillyauto.domain.QEmployee;
import com.oreillyauto.domain.QExample;
import com.oreillyauto.domain.QNickname;
import com.querydsl.core.group.GroupBy;

@Repository
public class ExampleRepositoryImpl extends QuerydslRepositorySupport implements ExampleRepositoryCustom {
	
	QExample exampleTable = QExample.example;
	QNickname nicknameTable = QNickname.nickname;
	QEmployee employeeTable = QEmployee.employee;
	
	public ExampleRepositoryImpl() {
		super(Example.class);
	}
	
    @SuppressWarnings("unchecked")
    @Override
    public List<Example> getExamples() {
		List<Example> eventList = (List<Example>) (Object) getQuerydsl().createQuery()
                .from(exampleTable)
                .fetch();
        return eventList;
    }
    
    @SuppressWarnings("unchecked")
    public void printExampleTable() {
        QExample exampleTable = QExample.example;
        
        List<Example> list = (List<Example>) (Object) getQuerydsl().createQuery()
                .from(exampleTable)
                .fetch();
        
        for (Object result : list) {
            System.out.println(result);
        }
    }
    
    public void printList(List<?> list) {
        for (Object result : list) {
            System.out.println(result);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Example> getExampleById(Integer id) {
        String sql = "SELECT * " + 
                     "  FROM examples " + 
                     " WHERE id = ?";
        Query query = getEntityManager().createNativeQuery(sql);
        query.setParameter(1, id); // NOTICE PARAMETERS ARE "1-BASED" and not "0-based"
        return (List<Example>)query.getResultList();
    }
    
    
    public Example getFirstExample() {
    	QExample exampleTable = QExample.example;
		Example example = (Example) getQuerydsl().createQuery()
                .from(exampleTable)
                .limit(1)
                .fetch();
        return example;
    }
    
    @SuppressWarnings("unchecked")
    public List<Example> getAllExamples() {
    	QExample exampleTable = QExample.example;
		List<Example> eventList = (List<Example>) (Object) getQuerydsl().createQuery()
                .from(exampleTable)
                .fetch();
        return eventList;
    }
	
	@Override
	public void testQueries(String day) {
		switch (day) {
		case "3":
			testDayThree();
			break;
		case "4":
			testDayFour();
			break;
		}
	}

	@SuppressWarnings("unchecked")
	private void testDayFour() {

		// EXAMPLE 1
		// INSERT Example - Typed SQL
		// Since we deleted all users with last_name = "Brannon" yesterday,
		// let's create a new one.
//        String sql = "INSERT INTO examples (first_name, last_name) " + 
//                     "VALUES ('Austin', 'Ates') ";
//        Query query = getEntityManager().createNativeQuery(sql);
//        query.executeUpdate();
//		
//		// Check to see if the insert was successful? What is the id
//		// for the new record?
//		List<Example> exampleList = from(exampleTable).fetch();
//		Helper.printList(exampleList);
		
		/**
			ResultTransformer
			* Querydsl provides a meaningful way to customize results using the
			  ResultTransformer for aggregation. 
			* The transformer works directly with the group by function to transform 
			  the data in memory. 
			* When working with group by, realize that we are working with aggregate 
			  functions and therefore end up with objects that do not necessarily 
			  relate to each other in any way 
			   - (i.e. they are not necessarily columns we would find in the table 
			      we are querying - like AVG, SUM, COUNT). 
			* This list of potentially disjointed data requires a Tuple.
			* Let's look at "Tuple" examples first, and then we will move to  
			  ResultTransformer examples.
		*/
		
		// EXAMPLE 2
        // Tuple example using a group by 
		// exampleTable.count() is our aggregate
		
		// We can build out our example per normal as seen below...
//        List<Tuple> list = (List<Tuple>) (Object) getQuerydsl().createQuery()
//            .select(exampleTable.lastName, exampleTable.count())
//            .from(exampleTable)
//            .groupBy(exampleTable.lastName)
//            .fetch();
		
		// OR - we can actually reduce the code (only when working with Tuples)
//        List<Tuple> list = 
//                from(exampleTable)
//                .select(exampleTable.lastName, exampleTable.count())
//                .groupBy(exampleTable.lastName)
//                .fetch();
//        printList(list);
        
		
		// EXAMPLE 3
		// Let's do another Tuple
//		List<Tuple> listAvgAge = (List<Tuple>) (Object) getQuerydsl().createQuery()
//                .select(employeeTable.lastName, employeeTable.age.avg())
//                .from(employeeTable)
//                .groupBy(employeeTable.lastName)
//                .fetch();
//            
//		printList(listAvgAge);
		
		
		/**
			* Now that we know how to query for columns that are not in our
			  table using a tuple, we need to look at something similar.
			* After grouping our data in certain ways, a standard List<Object>
			  does not work. We often aggregate our data and end up with a
			  "key" from the aggregate and some type of value (object or list).
			* We need a data type to handle key/value pairs..... A MAP!  
			* We can use what is called a Result Transformer to query for map.
			* Since we are working with aggregate data, we can transform the 
			  data into a java.util.Map, given that the aggregate column can 
			  become the key and hold some value.
		 */
		// EXAMPLE 4
        // Map - Transform Example - Group By Last Name
//        Map<String, Example> exampleMap = getQuerydsl().createQuery()
//                .from(exampleTable)
//                .where(exampleTable.id.in(
//                    JPAExpressions
//                    .select(exampleTable.id)
//                    .from(exampleTable)
//                    ))
//                .transform( GroupBy.groupBy(exampleTable.lastName) // this becomes the key
//                                   .as(exampleTable)               // this becomes the value
//                          );
//        
//        for (Map.Entry<String,Example> entry : exampleMap.entrySet()) {  
//            System.out.println("Key = " + entry.getKey() + 
//                             ", Value = " + entry.getValue());
//        }

		
		/**
		 * Let's work with the employee table now.
		 * Now that we got a map with last_name as the key and the record as the
		 * value, let's get the record as the key and the age as the value.
		 */
		// EXAMPLE 5
	    // Map - Transform Example – Group By Employee - Get ages 
		Map<Employee, Integer> employeeMap = getQuerydsl().createQuery()
				.from(employeeTable)
				.transform(
						GroupBy.groupBy(employeeTable)	// Key
						.as(employeeTable.age)          // Value
				);
		
		for (Map.Entry<Employee, Integer> entry : employeeMap.entrySet()) {
			System.out.println("Key = " + entry.getKey() + ", Value = " 
					+ entry.getValue());
		}
		
		
		
		
	}
	
	@SuppressWarnings("unchecked")
	public void testDayThree() {
		    	
    	// Example 1
    	// Simple example - retrieve one record 
		// This is the "LONG" way
//    	Example example = (Example) getQuerydsl().createQuery()
//    	           .from(exampleTable)
//    	           .limit(1)
//    	           .fetchOne();
//    	System.out.println("Example: " + example);

    	// -----------------------------------------------------------------

        // EXAMPLE 2
    	// Simple example - retrieve one record 
		// This is the "SHORT" way
//    	Example example2 = from(exampleTable)
//    		       .limit(1)
//    		       .fetchOne();
//    	System.out.println(example2);
    	
    	// -----------------------------------------------------------------    	
    	
    	// EXAMPLE 3
//    	List<Example> exampleList = from(exampleTable)
//                                  .fetch();
//    	Helper.printList(exampleList);		
            	
    	// -----------------------------------------------------------------  	
    	
        // EXAMPLE 4
        // java.persistence.Query takes a SQL string expression "SELECT * FROM <table>";
//		String sql = "SELECT * " + 
//					 "  FROM examples ";
//		Query query = getEntityManager().createNativeQuery(sql);
//		List<Example> exampleList = (List<Example>)(Object)query.getResultList();
//		Helper.printList(exampleList);

        // -----------------------------------------------------------------
        
        // EXAMPLE 5 
		// Select One "Jeffery"
//		Example exampleList = (Example) getQuerydsl().createQuery()
//					.from(exampleTable)
//					.where(exampleTable.firstName.eq("Jeffery"))
//					.fetchOne();
//        System.out.println(exampleList);
        
		// -----------------------------------------------------------------
		
		// EXAMPLE 6
		// Select users with first name = "Jeffery" 
//		List<Example> jefferyList = (List<Example>) (Object) getQuerydsl().createQuery()
//				.select(exampleTable.id, exampleTable.firstName, exampleTable.lastName)
//				.from(exampleTable)
//				.where(exampleTable.firstName.eq("Jeffery"))
//				.fetch();
//		Helper.printList(jefferyList);
        
		// -----------------------------------------------------------------
		
        // Example 7
		// Multiple sources (tables)
//        List<Object> exampleList = (List<Object>) (Object) getQuerydsl().createQuery()
//                .select(exampleTable.firstName, exampleTable.lastName, nicknameTable.nickName)
//                .from(exampleTable)
//                .innerJoin(nicknameTable)
//                .on(exampleTable.id.eq(nicknameTable.id))
//                .fetch();
//        Helper.printList(exampleList);
        
		// -----------------------------------------------------------------
		
        // Example 8
		// Multiple Filters - AND
//        List<Object> exampleList4 = (List<Object>) (Object) getQuerydsl().createQuery()
//                .select(exampleTable.firstName, exampleTable.lastName, nicknameTable.nickName)
//                .from(exampleTable)
//                .innerJoin(nicknameTable)
//                .on(exampleTable.id.eq(nicknameTable.id))
//                .where( exampleTable.firstName.equalsIgnoreCase("jeffery")
//                	   ,(nicknameTable.nickName.equalsIgnoreCase("giraffe"))
//                	  )
//                .fetch();  // Comma in the predicate is an "and"
//        Helper.printList(exampleList4);
        
		// -----------------------------------------------------------------
        
        // Example 9
		// Multiple Filters - OR
//        List<Object> exampleList4a = (List<Object>) (Object) getQuerydsl().createQuery()
//                .select(exampleTable.firstName, exampleTable.lastName, nicknameTable.nickName)
//                .from(exampleTable)
//                .innerJoin(nicknameTable)
//                .on(exampleTable.id.eq(nicknameTable.id))
//                .where(
//                		nicknameTable.nickName.eq("giraffe")
//                		 .or(nicknameTable.nickName.eq("dawg"))
//                	  ) // another example using or instead of and
//                .fetch();
//        Helper.printList(exampleList4a);
		
		// -----------------------------------------------------------------
        
        // Example 10 
		// Left Join
//        List<Object> exampleList5 = (List<Object>) (Object) getQuerydsl().createQuery()
//                .select(exampleTable.firstName, exampleTable.lastName, nicknameTable.nickName)
//                .from(exampleTable)
//                .leftJoin(nicknameTable)
//                .on(exampleTable.id.eq(nicknameTable.id))
//                .fetch();
//        Helper.printList(exampleList5);
        
		// -----------------------------------------------------------------
        
        // Example 11 
		// Ordering 
//        List<Example> exampleList6 = (List<Example>) (Object) getQuerydsl().createQuery()
//                .select(exampleTable.firstName, exampleTable.lastName)
//                .from(exampleTable)
//                .orderBy(exampleTable.firstName.asc())
//                .fetch();
//        Helper.printList(exampleList6);
        
		// -----------------------------------------------------------------
        
        // Example 12
		// Grouping
//        List<Example> exampleList7 = (List<Example>) (Object) getQuerydsl().createQuery()
//                .select(exampleTable.lastName)
//                .from(exampleTable)
//                .groupBy(exampleTable.lastName)
//                //.having()
//                .fetch();
//        Helper.printList(exampleList7);
        
		// -----------------------------------------------------------------
		
        // Example 13
		// Delete
//        EntityManager em = getEntityManager();
//        em.joinTransaction();
//        long deletedItemsCount = new JPADeleteClause(em, exampleTable)
//                .where(exampleTable.lastName.eq("Brannon"))
//                .execute();
//        
//        System.out.println(deletedItemsCount + " records deleted.");

		// Now that we have potentially deleted some records, let's query the entire
        // table and print the results
//        List<Example> exampleList8 = (List<Example>) (Object) getQuerydsl().createQuery()
//                .from(exampleTable)
//                .fetch();
//        
//        for (Object result : exampleList8) {
//            System.out.println(result);
//        }

		// -----------------------------------------------------------------
		
        // Example 14
		// Update
//		EntityManager em = getEntityManager();
//		em.joinTransaction();
//        //long updatedCount = new JPAUpdateClause(getEntityManager(), exampleTable)
//		long updatedCount = new JPAUpdateClause(em, exampleTable)
//                .set(exampleTable.lastName, "Dawg")
//                .where(exampleTable.lastName.toLowerCase().eq("sutton"))
//                .execute();
//        System.out.println(updatedCount + " records updated.");
//        printExampleTable();

		// -----------------------------------------------------------------
		
         // Example 15 
		 // Sub Query					
//            Object obj = getQuerydsl().createQuery()
//                    .from(exampleTable)
//                    .where(
//                    		exampleTable.id.in(
//                              JPAExpressions.select(
//                        		exampleTable.id.max())
//                                .from(exampleTable)
//                            )
//                    )
//                    .fetchOne();    
//            
//            System.out.println("obj=> "  + obj);
		
	}
	
    @SuppressWarnings("unchecked")
    @Override
    public List<Object> getTables() {
        String sql = "  SELECT st.tablename " + 
        	  	     "    FROM sys.systables st " + 
        		     "   WHERE st.tablename NOT LIKE 'SYS%'" +
        		     "ORDER BY st.tablename";
        Query query = getEntityManager().createNativeQuery(sql);
        return (List<Object>)query.getResultList();
    }
	
}
